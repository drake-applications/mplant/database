/*
 * Copyright (c) 2003 - 2018 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "MSQStringEscaper.h"
#include <sqlite3.h>

// --------------------------------------------------------------
//  Escape
// --------------------------------------------------------------
// SQLite implementation of escaping a query string

MString MSQStringEscaper::Escape(const MString& inString)
{
  char* escapedStr = sqlite3_mprintf("%Q", inString.c_str());
  return MString(escapedStr);
}

// --------------------------------------------------------------
//  NullValue
// --------------------------------------------------------------
// How to convert the null value to a string in SQLite

MString MSQStringEscaper::NullValue(void)
{
  return MString("NULL");
}

