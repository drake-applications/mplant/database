/*
 * Copyright (c) 2003 - 2018 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _MSQSTATEMENT_H_
#define _MSQSTATEMENT_H_

#include "MDBStatement.h"
#include <sqlite3.h>

class MSQStatement : public MDBStatement
{
  public:
    MSQStatement(sqlite3* inDatabase);
    virtual ~MSQStatement(void);
    virtual MString EscapeString(const MString& inString);
  protected:
    virtual MDBResultSet* ExecuteQuery_(const MString& inQuery);
    virtual MDBResultSet* ExecuteQuery_(void);
    virtual void Prepare_(const MString& inQuery);
    virtual void Execute_(const MString& inQuery);

    virtual void BindString_(int inIndex, const MString& inString);
    virtual void BindInteger_(int inIndex, const MInteger& inString);
    virtual void BindFloat_(int inIndex, const MFloat& inString);
    virtual void Begin_(void);
    virtual void Rollback_(void);
    virtual void Commit_(void);

    void CheckBindResult(int inResult);
    sqlite3* mDatabase;
    sqlite3_stmt* mStatement;
};

#endif

