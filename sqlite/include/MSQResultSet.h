/*
 * Copyright (c) 2003 - 2018 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _MSQRESULTSET_H_
#define _MSQRESULTSET_H_

#include "MDBResultSet.h"
#include <sqlite3.h>

class MSQResultSet : public MDBResultSet
{
  public:
    MSQResultSet(sqlite3* inDatabase, sqlite3_stmt* inStatement);
    virtual ~MSQResultSet() {}
    virtual bool First(void);
    virtual bool Next(void);
    virtual bool HasColumnNamed(const MString& inColumn);
    virtual MIndex IndexOfColumnNamed(const MString& inColumn);
    virtual MString GetStringAtIndex(MIndex inIndex);
    virtual bool GetStringAtIndex(MIndex inIndex, MString& outString);
    virtual MInteger GetIntAtIndex(MIndex inIndex);
    virtual MFloat GetFloatAtIndex(MIndex inIndex);
    virtual bool GetDataAtIndex(MIndex inIndex, MVector<char>& outData);
    // TODO Date and Time
    virtual bool GetBoolAtIndex(MIndex inIndex);

    //virtual MInteger GetLastInsertKey(void); // Needed?
  protected:
    void Execute(void);
    sqlite3* mDatabase;
    sqlite3_stmt* mStatement;
};

#endif

