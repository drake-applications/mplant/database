/*
 * Copyright (c) 2003 - 2018 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "MSQConnection.h"
#include "MSQStatement.h"
#include "UString.h"

// --------------------------------------------------------------
//  Constructor
// --------------------------------------------------------------
// Construct a new SQLite connection to the given file

MSQConnection::MSQConnection(const MString& inUrl) : MDBConnection(inUrl)
{
  MString url = UString::ReplaceOnce(inUrl, "sqlite://", "");
  int result = sqlite3_open(url.c_str(), &mDatabase);
  if (result == SQLITE_OK)
  {
    mConnected = true;
  }
}

// --------------------------------------------------------------
//  Destructor
// --------------------------------------------------------------
// Destroy the SQLite connection

MSQConnection::~MSQConnection()
{
  Close();
}

// --------------------------------------------------------------
//  Close
// --------------------------------------------------------------
// Close the SQLite connection cleanly

void MSQConnection::Close(void)
{
  if (mConnected)
  {
    if (mDatabase)
    {
      sqlite3_close(mDatabase);
      mDatabase = kNull;
    }
    mConnected = false;
  }
}

// --------------------------------------------------------------
//  CreateStatement
// --------------------------------------------------------------
// Create a database statement for the SQLite connection

MDBStatement* MSQConnection::CreateStatement(void)
{
  MDBStatement* statement = kNull;

  if (mConnected && mDatabase)
  {
    statement = new MSQStatement(mDatabase);
  }
  return statement;
}

