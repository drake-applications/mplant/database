/*
 * Copyright (c) 2003 - 2018 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "MSQResultSet.h"
#include <stdexcept>

// --------------------------------------------------------------
//  Constructor
// --------------------------------------------------------------
// Create a new SQLite result set

MSQResultSet::MSQResultSet(sqlite3* inDatabase, sqlite3_stmt* inStatement)
{
  mDatabase = inDatabase;
  mStatement = inStatement;
  Execute();
}

// --------------------------------------------------------------
//  Execute
// --------------------------------------------------------------
// Perform the statement execution to populate the result set

void MSQResultSet::Execute(void)
{
  int result = sqlite3_step(mStatement);
  if (result == SQLITE_ERROR)
  { 
    MString message = sqlite3_errmsg(mDatabase);
    throw std::runtime_error(message);
  }
  else
  { 
    sqlite3_reset(mStatement);
  }
}

// --------------------------------------------------------------
//  First
// --------------------------------------------------------------
// Go to the initial result

bool MSQResultSet::First(void)
{
  sqlite3_reset(mStatement);
  return Next();
}

// --------------------------------------------------------------
//  Next
// --------------------------------------------------------------
// Go to the next result

bool MSQResultSet::Next(void)
{
  bool hasNext = false;

  int result = sqlite3_step(mStatement);
  if (result == SQLITE_ROW)
  { 
    hasNext = true;
  }
  else if (result == SQLITE_DONE)
  { 
    hasNext = false;
  }
  else if (result == SQLITE_ERROR)
  { 
    MString message = sqlite3_errmsg(mDatabase);
    throw std::runtime_error(message);
  }

  return hasNext;
}

// --------------------------------------------------------------
//  HasColumnNamed
// --------------------------------------------------------------
// Determine if there is a column in the result set with the
// given name

bool MSQResultSet::HasColumnNamed(const MString& inColumn)
{
  return IndexOfColumnNamed(inColumn) != kNotFound;
}

// --------------------------------------------------------------
//  IndexOfColumnNamed
// --------------------------------------------------------------
// Get the index of the named column

MIndex MSQResultSet::IndexOfColumnNamed(const MString& inColumn)
{
  bool found = false;
  MIndex index;
  
  MSize numCols = sqlite3_column_count(mStatement);
  
  for (MIndex col = 0; !found && col < numCols; col++)
  {
    const char* utfCol = sqlite3_column_name(mStatement, col);
    MString strCol = utfCol;
    
    found = (strCol == inColumn);

    if (found)
    {
      index = col;
    }
  }

  if (!found)
  {
    index = kNotFound;
  }
 
  return index;
}

// --------------------------------------------------------------
//  GetStringAtIndex
// --------------------------------------------------------------
// Get the string value at a given column index of the current
// result

MString MSQResultSet::GetStringAtIndex(MIndex index)
{
  MString string;

  const char* text = (const char*)sqlite3_column_text(mStatement, index);
  if (text)
  {
    string = text;
  }

  return string;
}

// --------------------------------------------------------------
//  GetStringAtIndex
// --------------------------------------------------------------
// Get the string value at a given column index of the current
// result, assigning the string by reference

bool MSQResultSet::GetStringAtIndex(MIndex index, MString& outString)
{
  bool success = false;

  if (index != kNotFound)
  {
    const char* text = (const char*)sqlite3_column_text(mStatement, index);
    if (text)
    {
      outString = text;
      success = true;
    }
  }

  return success;
}

// --------------------------------------------------------------
//  GetIntAtIndex
// --------------------------------------------------------------
// Get the integer value at a given column index of the current
// result

MInteger MSQResultSet::GetIntAtIndex(MIndex index)
{
  if (index == kNotFound)
  {
    return 0;
  }
  return sqlite3_column_int(mStatement, index);
}

// --------------------------------------------------------------
//  GetFloatAtIndex
// --------------------------------------------------------------
// Get the float value at a given column index of the current
// result

MFloat MSQResultSet::GetFloatAtIndex(MIndex index)
{
  if (index == kNotFound)
  {
    return 0;
  }
  return sqlite3_column_double(mStatement, index);
}

// --------------------------------------------------------------
//  GetBoolAtIndex
// --------------------------------------------------------------
// Get the boolean value at a given column index of the current
// result

bool MSQResultSet::GetBoolAtIndex(MIndex index)
{
  return GetIntAtIndex(index) > 0;
}

// --------------------------------------------------------------
//  GetDataAtIndex
// --------------------------------------------------------------
// Get the data value at a given column index of the current
// result, by reference

bool MSQResultSet::GetDataAtIndex(MIndex index, MVector<char>& outData)
{
  bool success = false;

  if (index != kNotFound)
  {
    MSize length = sqlite3_column_bytes(mStatement, index);
  
    if (length > 0)
    {
      outData.resize(length);
      const char* blob = reinterpret_cast<const char*>(sqlite3_column_blob(mStatement, index));
      std::copy(blob, blob + outData.size(), &outData[0]);
      success = true;
    }
  }

  return success;
}

