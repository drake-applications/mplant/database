/*
 * Copyright (c) 2003 - 2018 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "MSQStatement.h"
#include "MSQResultSet.h"
#include <stdexcept>

// --------------------------------------------------------------
//  Constructor
// --------------------------------------------------------------
// Construct an SQLite statement

MSQStatement::MSQStatement(sqlite3* inDatabase)
{
  mDatabase = inDatabase;
  mStatement = kNull;
}

// --------------------------------------------------------------
//  Destructor
// --------------------------------------------------------------
// Cleanly destroy an SQLite statement

MSQStatement::~MSQStatement(void)
{
  sqlite3_finalize(mStatement);
}

// --------------------------------------------------------------
//  ExecuteQuery_
// --------------------------------------------------------------
// The SQLite implementation for executing a query that returns
// a result set

MDBResultSet* MSQStatement::ExecuteQuery_(const MString& inQuery)
{
  MDBResultSet* resultSet = kNull;
  int result = 0;

  if (inQuery.length() > 0)
  {
    if (mStatement)
    {
      sqlite3_finalize(mStatement);
      mStatement = kNull;
    }

    result = sqlite3_prepare_v2(mDatabase, inQuery.c_str(), inQuery.length(), &mStatement, kNull);

    if (result == SQLITE_OK)
    {
      resultSet = new MSQResultSet(mDatabase, mStatement);
    }
    else
    {
      MString message = sqlite3_errmsg(mDatabase);
      if (message.length() > 0)
      {
        throw std::runtime_error(message);
      }
    }
  }

  return resultSet;  
}

// --------------------------------------------------------------
//  Execute_
// --------------------------------------------------------------
// The SQLite implementation for executing a query that does not
// return a result set

void MSQStatement::Execute_(const MString& inQuery)
{
  if (mStatement)
  {
    sqlite3_finalize(mStatement);
    mStatement = NULL;
  }

  char* errorMsg = NULL;
  sqlite3_exec(mDatabase, inQuery.c_str(), NULL, NULL, &errorMsg);
  if (errorMsg)
  {
    MString message = errorMsg;
    sqlite3_free(errorMsg);
    throw std::runtime_error("Failed to execute sql: " + message);
  }
}

// --------------------------------------------------------------
//  Prepare_
// --------------------------------------------------------------
// The SQLite implementation for creating a prepared query

void MSQStatement::Prepare_(const MString& inQuery)
{
  if (mStatement)
  {
    sqlite3_finalize(mStatement);
    mStatement = kNull;
  }

  if (sqlite3_prepare_v2(mDatabase, inQuery.c_str(), -1, &mStatement, NULL))
  {
    throw std::runtime_error(MString("Failed to prepare query: ") + sqlite3_errmsg(mDatabase));
  }
}

// --------------------------------------------------------------
//  Execute_
// --------------------------------------------------------------
// The SQLite implementation for executing a prepared query

MDBResultSet* MSQStatement::ExecuteQuery_(void)
{
  MDBResultSet* results = kNull;
  MInteger result = sqlite3_step(mStatement);

  if (result == SQLITE_DONE)
  {
    sqlite3_reset(mStatement);
  }
  else if (result == SQLITE_ROW)
  {
    results = new MSQResultSet(mDatabase, mStatement);
  }
  else
  {
    throw std::runtime_error(MString("Failed to execute query: ") + sqlite3_errmsg(mDatabase));
  }

  return results;
}

// --------------------------------------------------------------
//  EscapeString
// --------------------------------------------------------------
// The SQLite implementation for escaping a query string

MString MSQStatement::EscapeString(const MString& inString)
{
  char* escapedStr = sqlite3_mprintf("%Q", inString.c_str());
  return MString(escapedStr);
}

// --------------------------------------------------------------
//  BindString_
// --------------------------------------------------------------
// The SQLite implementation for binding a string at an index

void MSQStatement::BindString_(int inIndex, const MString& inString)
{
  int result = 0;

  if (inString.empty())
  {
    result = sqlite3_bind_null(mStatement, inIndex);
  }
  else
  {
    result = sqlite3_bind_text(mStatement, inIndex, inString.c_str(), inString.length(), SQLITE_STATIC);
  }
  CheckBindResult(result);
}

// --------------------------------------------------------------
//  BindInteger_
// --------------------------------------------------------------
// The SQLite implementation for binding an integer at an index

void MSQStatement::BindInteger_(int inIndex, const MInteger& inInteger)
{
  CheckBindResult(sqlite3_bind_int64(mStatement, inIndex, inInteger));
}

// --------------------------------------------------------------
//  BindFloat_
// --------------------------------------------------------------
// The SQLite implementation for binding a float at an index

void MSQStatement::BindFloat_(int inIndex, const MFloat& inFloat)
{
  CheckBindResult(sqlite3_bind_double(mStatement, inIndex, inFloat));
}

// --------------------------------------------------------------
//  CheckBindResult
// --------------------------------------------------------------
// Verify that value binding was successful (SQLite-specific)

void MSQStatement::CheckBindResult(int inResult)
{
  if (inResult != SQLITE_OK)
  {
    throw std::runtime_error("Failed to bind value");
  }
}

// --------------------------------------------------------------
//  Begin_
// --------------------------------------------------------------
// The SQLite implementation of starting a transaction

void MSQStatement::Begin_(void)
{
  Execute("BEGIN");
}

// --------------------------------------------------------------
//  Rollback_
// --------------------------------------------------------------
// The SQLite implementation of rolling back a transaction

void MSQStatement::Rollback_(void)
{
  Execute("ROLLBACK");
}

// --------------------------------------------------------------
//  Commit_
// --------------------------------------------------------------
// The SQLite implementation of committing a transaction

void MSQStatement::Commit_(void)
{
  Execute("COMMIT");
}

