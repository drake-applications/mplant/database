/*
 * Copyright (c) 2003 - 2018 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "MDBStatement.h"
#include "MTypes.h"
#include "MLogger.h"

// --------------------------------------------------------------
//  ExecuteQuery
// --------------------------------------------------------------
// Execute a query that returns a result set.  The query is a
// parameterized meta string that will be evaluated with the
// given parameters

MDBResultSet* MDBStatement::ExecuteQuery(const MMetaString& inQuery, const MParamMap& inParams)
{
  return ExecuteQuery(inQuery.Evaluate(inParams));
}

// --------------------------------------------------------------
//  Execute
// --------------------------------------------------------------
// Execute a query that does not return a result set.  The query
// is a parameterized meta string that will be evaluated with the
// given parameters

void MDBStatement::Execute(const MMetaString& inQuery, const MParamMap& inParams)
{
  Execute(inQuery.Evaluate(inParams));
}

// --------------------------------------------------------------
//  Prepare
// --------------------------------------------------------------
// Create a prepared query that does not return a result set.

void MDBStatement::Prepare(const MMetaString& inQuery, const MParamMap& inParams)
{
  Prepare(inQuery.Evaluate(inParams));
}

// --------------------------------------------------------------
//  ExecuteQuery
// --------------------------------------------------------------
// Execute a query string that returns a result set.

MDBResultSet* MDBStatement::ExecuteQuery(const MString& inQuery)
{
  MLogger::RootLogger().Trace("Executing Query:\n" + inQuery);
  return ExecuteQuery_(inQuery);
}

// --------------------------------------------------------------
//  ExecuteQuery
// --------------------------------------------------------------
// Execute a prepared query that returns a result set.

MDBResultSet* MDBStatement::ExecuteQuery(void)
{
  MLogger::RootLogger().Trace("Executing prepared query");
  return ExecuteQuery_();
}

// --------------------------------------------------------------
//  Prepare
// --------------------------------------------------------------
// Create a prepared query from the query string.

void MDBStatement::Prepare(const MString& inQuery)
{
  MLogger::RootLogger().Trace("Preparing Query:\n" + inQuery);
  Prepare_(inQuery);
}

// --------------------------------------------------------------
//  Execute
// --------------------------------------------------------------
// Execute the given query string without returning a result set.

void MDBStatement::Execute(const MString& inQuery)
{
  MLogger::RootLogger().Trace("Executing Query:\n" + inQuery);
  Execute_(inQuery);
}

// --------------------------------------------------------------
//  BindString
// --------------------------------------------------------------
// Bind a string to the prepared statement at a given index.

void MDBStatement::BindString(int inIndex, const MString& inString)
{
  MLogger::RootLogger().Trace("Binding String: " + inString);
  BindString_(inIndex, inString);
}

// --------------------------------------------------------------
//  BindInteger
// --------------------------------------------------------------
// Bind a integer to the prepared statement at a given index.

void MDBStatement::BindInteger(int inIndex, const MInteger& inInteger)
{
  MLogger::RootLogger().Trace("Binding Integer: " + std::to_string(inInteger));
  BindInteger_(inIndex, inInteger);
}

// --------------------------------------------------------------
//  BindFloat
// --------------------------------------------------------------
// Bind a float to the prepared statement at a given index.

void MDBStatement::BindFloat(int inIndex, const MFloat& inFloat)
{
  MLogger::RootLogger().Trace("Binding Float: " + std::to_string(inFloat));
  BindFloat_(inIndex, inFloat);
}

// --------------------------------------------------------------
//  Begin
// --------------------------------------------------------------
// Begin a transaction

void MDBStatement::Begin(void)
{
  MLogger::RootLogger().Trace("Beginning Transaction");
  Begin_();
}

// --------------------------------------------------------------
//  Rollback
// --------------------------------------------------------------
// Rollback the current transaction

void MDBStatement::Rollback(void)
{
  MLogger::RootLogger().Trace("Rolling Back Transaction");
  Rollback_();
}

// --------------------------------------------------------------
//  Commit
// --------------------------------------------------------------
// Commit the current transaction

void MDBStatement::Commit(void)
{
  MLogger::RootLogger().Trace("Committing Transaction");
  Commit_();
}
