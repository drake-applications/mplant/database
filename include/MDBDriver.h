/*
 * Copyright (c) 2003 - 2018 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _MDBDRIVER_H_
#define _MDBDRIVER_H_

#include "MDBConnection.h"

class MDBDriver
{
  public:
    virtual ~MDBDriver(void) {}
    virtual MDBConnection* GetConnection(const std::string& inUrl) = 0;
    virtual MString GetProtocol(void) const = 0;
};

#endif

