/*
 * Copyright (c) 2003 - 2018 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _MDBSTATEMENT_H_
#define _MDBSTATEMENT_H_

#include "MDBResultSet.h"
#include "MMetaString.h"

class MDBStatement
{
  public:
    virtual ~MDBStatement(void) {}
    MDBResultSet* ExecuteQuery(const MString& inQuery);
    MDBResultSet* ExecuteQuery(void);
    void Prepare(const MString& inQuery);
    void Execute(const MString& inQuery);

    MDBResultSet* ExecuteQuery(const MMetaString& inQuery, const MParamMap& inParams);
    void Execute(const MMetaString& inQuery, const MParamMap& inParams);
    void Prepare(const MMetaString& inQuery, const MParamMap& inParams);

    void BindString(int inIndex, const MString& inString);
    void BindInteger(int inIndex, const MInteger& inInteger);
    void BindFloat(int inIndex, const MFloat& inFloat);

    void Begin(void);
    void Rollback(void);
    void Commit(void);

    virtual MString EscapeString(const MString& inString) = 0;
  protected:
    virtual MDBResultSet* ExecuteQuery_(const MString& inQuery) = 0;
    virtual MDBResultSet* ExecuteQuery_(void) = 0;
    virtual void Prepare_(const MString& inQuery) = 0;
    virtual void Execute_(const MString& inQuery) = 0;

    virtual void BindString_(int inIndex, const MString& inString) = 0;
    virtual void BindInteger_(int inIndex, const MInteger& inInteger) = 0;
    virtual void BindFloat_(int inIndex, const MFloat& inFloat) = 0;

    virtual void Begin_(void) = 0;
    virtual void Rollback_(void) = 0;
    virtual void Commit_(void) = 0;
};

#endif

