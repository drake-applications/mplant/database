/*
 * Copyright (c) 2003 - 2018 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef _MDBRESULTSET_H_
#define _MDBRESULTSET_H_

#include "MTypes.h"

class MDBResultSet
{
  public:
    virtual ~MDBResultSet(void) {}
    virtual bool First(void) = 0;
    virtual bool Next(void) = 0;
    virtual bool HasColumnNamed(const MString& inColumn) = 0;
    virtual MIndex IndexOfColumnNamed(const MString& inColumn) = 0;
    virtual MString GetStringAtIndex(MIndex inIndex) = 0;
    virtual MString GetStringAtColumn(const MString& inColumn)  { return GetStringAtIndex(IndexOfColumnNamed(inColumn)); }
    virtual bool GetStringAtIndex(MIndex inIndex, MString& outString) = 0;
    virtual bool GetStringAtColumn(const MString& inColumn, MString& outString) { return GetStringAtIndex(IndexOfColumnNamed(inColumn), outString); }
    virtual MInteger GetIntAtIndex(MIndex inIndex) = 0;
    virtual MInteger GetIntAtColumn(const MString& inColumn) { return GetIntAtIndex(IndexOfColumnNamed(inColumn)); }
    virtual MFloat GetFloatAtIndex(MIndex inIndex) = 0;
    virtual MFloat GetFloatAtColumn(const MString& inColumn) { return GetFloatAtIndex(IndexOfColumnNamed(inColumn)); }
    virtual bool GetDataAtIndex(MIndex inIndex, MVector<char>& outData) = 0;
    virtual bool GetDataAtColumn(const MString& inColumn, MVector<char>& outData) { return GetDataAtIndex(IndexOfColumnNamed(inColumn), outData); }
    // TODO Date and Time
    virtual bool GetBoolAtIndex(MIndex inIndex) = 0;
    virtual bool GetBoolAtColumn(const MString& inColumn) { return GetBoolAtIndex(IndexOfColumnNamed(inColumn)); }

    //virtual MInteger GetLastInsertKey(void) = 0; // Needed?
};

#endif

