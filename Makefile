#
# Copyright (c) 2003 - 2018 by Drake Applications
# 
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
# 
#     http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

TARGET = libMPlantDatabase.so

CC = gcc
CXX = g++
CP = cp
RM = rm -f
MKDIR = mkdir -p

INSTALL_PATH=../bin

CPPFLAGS = -I include -I sqlite/include -I ../core/include -fPIC -Wall -Wextra -g -std=c++11
           
LDFLAGS = -shared -L ../core/ -l MPlantCore -l sqlite3
LIBS=
OBJDIR=obj
SQLOBJDIR=obj/sqlite
PSQLOBJDIR=obj/psql

SRCS = $(wildcard *.cpp) $(wildcard sqlite/*.cpp)
OBJS = $(addprefix $(OBJDIR)/, $(subst .cpp,.o,$(SRCS)))

default: $(OBJDIR) $(SQLOBJDIR) $(TARGET) install
all: default

$(TARGET): $(OBJS)
	$(CXX) $(LDFLAGS) -o $(TARGET) $(OBJS) $(LIBS)

$(OBJDIR):
	$(MKDIR) $(OBJDIR)

$(SQLOBJDIR):
	$(MKDIR) $(SQLOBJDIR)

$(OBJDIR)/%.o: %.cpp
	$(CXX) $(CPPFLAGS) -c $< -o $@

$(SQLOBJDIR)/%o: %.cpp
	$(CSS) $(CPPFLAGS) -c $< -o $@

depend: .depend

.depend: $(SRCS)
	$(RM) ./.depend
	$(CXX) $(CPPFLAGS) -MM $^ >>./.depend;

install:
	$(MKDIR) ../bin
	$(CP) $(TARGET) $(INSTALL_PATH)

clean:
	$(RM) $(OBJS)

distclean: clean
	$(RM) *~ .depend

include .depend
