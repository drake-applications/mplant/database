/*
 * Copyright (c) 2003 - 2018 by Drake Applications
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "MDBC.h"
#include "MTypes.h"
#include "UString.h"

MMap<MString, TReference<MDBDriver>> MDBC::_drivers;

// --------------------------------------------------------------
//  Add Driver
// --------------------------------------------------------------
// Map a database driver for the given protocol

void MDBC::AddDriver(MDBDriver* inDriver)
{
  MDBC::_drivers[inDriver->GetProtocol()] = inDriver;
}

// --------------------------------------------------------------
//  HasDriverForProtocol
// --------------------------------------------------------------
// Map a database driver for the given protocol

bool MDBC::HasDriverForProtocol(MString inProtocol)
{
  return MMAP_EXISTS(MDBC::_drivers, inProtocol);
}

// --------------------------------------------------------------
//  GetConnectionWithURL
// --------------------------------------------------------------
// Get a database connection using the appropriate registered
// driver for the given database, according to the protocol
// contained in the URL.

MDBConnection* MDBC::GetConnectionWithURL(const MString& inUrl)
{
  MDBConnection* connection = kNull;
  TReference<MDBDriver> driver;

  MStringVector parts = UString::Parse(inUrl, ":");
  MString protocol = parts[0];

  if (MDBC::HasDriverForProtocol(protocol))
  {
    driver = MMAP_VALUE(MDBC::_drivers, protocol);
    connection = driver->GetConnection(inUrl);
  }

  return connection;
}

